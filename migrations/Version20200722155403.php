<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200722155403 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE abonnement (id INT AUTO_INCREMENT NOT NULL, formule_id INT DEFAULT NULL, ecole_id INT NOT NULL, prix DOUBLE PRECISION NOT NULL, canceled_date DATETIME NOT NULL, replicate_remise DOUBLE PRECISION DEFAULT NULL, remise DOUBLE PRECISION DEFAULT NULL, stripe_suscription VARCHAR(255) DEFAULT NULL, is_resilie TINYINT(1) DEFAULT NULL, stripe_customer VARCHAR(255) DEFAULT NULL, date_debut DATETIME DEFAULT NULL, expired TINYINT(1) DEFAULT NULL, code_visio VARCHAR(255) DEFAULT NULL, INDEX IDX_351268BB2A68F4D1 (formule_id), INDEX IDX_351268BB77EF1B1E (ecole_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE classe (id INT AUTO_INCREMENT NOT NULL, ecole_id INT NOT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_8F87BF9677EF1B1E (ecole_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cour (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cycle (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devoir (id INT AUTO_INCREMENT NOT NULL, matiere_id INT DEFAULT NULL, type INT DEFAULT NULL, date DATETIME NOT NULL, sujet LONGTEXT DEFAULT NULL, corrige LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, titre VARCHAR(255) NOT NULL, moyenne_generale DOUBLE PRECISION DEFAULT NULL, note DOUBLE PRECISION DEFAULT NULL, INDEX IDX_749EA771F46CD258 (matiere_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devoir_user (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, devoir_id INT NOT NULL, copy LONGTEXT DEFAULT NULL, note DOUBLE PRECISION DEFAULT NULL, mention VARCHAR(255) DEFAULT NULL, INDEX IDX_78666799A76ED395 (user_id), INDEX IDX_78666799C583534E (devoir_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ecole (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, code_uai VARCHAR(255) NOT NULL, adresse VARCHAR(255) NOT NULL, rue VARCHAR(255) DEFAULT NULL, ville VARCHAR(255) NOT NULL, code_postal VARCHAR(255) DEFAULT NULL, pays VARCHAR(255) NOT NULL, telephone VARCHAR(255) DEFAULT NULL, fixe VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formule (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prix DOUBLE PRECISION NOT NULL, stripe_product VARCHAR(255) DEFAULT NULL, duree INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE jumellage (id INT AUTO_INCREMENT NOT NULL, ecole_id INT NOT NULL, confirme TINYINT(1) DEFAULT NULL, date_invitation DATETIME NOT NULL, date_validation DATETIME DEFAULT NULL, titre VARCHAR(255) DEFAULT NULL, jumelle INT NOT NULL, INDEX IDX_AEF28FB77EF1B1E (ecole_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE litige (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, titre VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, date DATETIME NOT NULL, documents LONGTEXT NOT NULL, traite TINYINT(1) NOT NULL, INDEX IDX_EEE9D46DA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE matiere (id INT AUTO_INCREMENT NOT NULL, cycle_id INT NOT NULL, classe_id INT NOT NULL, nom VARCHAR(255) NOT NULL, moyenne DOUBLE PRECISION NOT NULL, mentions LONGTEXT DEFAULT NULL, note DOUBLE PRECISION NOT NULL, coefficient INT NOT NULL, INDEX IDX_9014574A5EC1162 (cycle_id), INDEX IDX_9014574A8F5EA509 (classe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(255) NOT NULL, text VARCHAR(255) NOT NULL, document LONGTEXT DEFAULT NULL, is_read TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification_user (notification_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_35AF9D73EF1A9D84 (notification_id), INDEX IDX_35AF9D73A76ED395 (user_id), PRIMARY KEY(notification_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, ecole_id INT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, date_naissance DATETIME NOT NULL, titre VARCHAR(255) NOT NULL, role VARCHAR(255) NOT NULL, INDEX IDX_8D93D64977EF1B1E (ecole_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_visioconference (user_id INT NOT NULL, visioconference_id INT NOT NULL, INDEX IDX_C0546ECCA76ED395 (user_id), INDEX IDX_C0546ECCC5C89FEB (visioconference_id), PRIMARY KEY(user_id, visioconference_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_classe (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, classe_id INT NOT NULL, date_inscription DATETIME NOT NULL, INDEX IDX_EAD5A4ABA76ED395 (user_id), INDEX IDX_EAD5A4AB8F5EA509 (classe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE visioconference (id INT AUTO_INCREMENT NOT NULL, classe_id INT DEFAULT NULL, type INT NOT NULL, date_creation DATETIME NOT NULL, confirme INT DEFAULT NULL, date_programme DATETIME DEFAULT NULL, duree INT DEFAULT NULL, date_fin DATETIME DEFAULT NULL, capacite INT NOT NULL, initiateur INT NOT NULL, ecole INT NOT NULL, jumelle INT DEFAULT NULL, INDEX IDX_D28802438F5EA509 (classe_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE abonnement ADD CONSTRAINT FK_351268BB2A68F4D1 FOREIGN KEY (formule_id) REFERENCES formule (id)');
        $this->addSql('ALTER TABLE abonnement ADD CONSTRAINT FK_351268BB77EF1B1E FOREIGN KEY (ecole_id) REFERENCES ecole (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE classe ADD CONSTRAINT FK_8F87BF9677EF1B1E FOREIGN KEY (ecole_id) REFERENCES ecole (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE devoir ADD CONSTRAINT FK_749EA771F46CD258 FOREIGN KEY (matiere_id) REFERENCES matiere (id)');
        $this->addSql('ALTER TABLE devoir_user ADD CONSTRAINT FK_78666799A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE devoir_user ADD CONSTRAINT FK_78666799C583534E FOREIGN KEY (devoir_id) REFERENCES devoir (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE jumellage ADD CONSTRAINT FK_AEF28FB77EF1B1E FOREIGN KEY (ecole_id) REFERENCES ecole (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE litige ADD CONSTRAINT FK_EEE9D46DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE matiere ADD CONSTRAINT FK_9014574A5EC1162 FOREIGN KEY (cycle_id) REFERENCES cycle (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE matiere ADD CONSTRAINT FK_9014574A8F5EA509 FOREIGN KEY (classe_id) REFERENCES classe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notification_user ADD CONSTRAINT FK_35AF9D73EF1A9D84 FOREIGN KEY (notification_id) REFERENCES notification (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notification_user ADD CONSTRAINT FK_35AF9D73A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64977EF1B1E FOREIGN KEY (ecole_id) REFERENCES ecole (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_visioconference ADD CONSTRAINT FK_C0546ECCA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_visioconference ADD CONSTRAINT FK_C0546ECCC5C89FEB FOREIGN KEY (visioconference_id) REFERENCES visioconference (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_classe ADD CONSTRAINT FK_EAD5A4ABA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_classe ADD CONSTRAINT FK_EAD5A4AB8F5EA509 FOREIGN KEY (classe_id) REFERENCES classe (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE visioconference ADD CONSTRAINT FK_D28802438F5EA509 FOREIGN KEY (classe_id) REFERENCES classe (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE matiere DROP FOREIGN KEY FK_9014574A8F5EA509');
        $this->addSql('ALTER TABLE user_classe DROP FOREIGN KEY FK_EAD5A4AB8F5EA509');
        $this->addSql('ALTER TABLE visioconference DROP FOREIGN KEY FK_D28802438F5EA509');
        $this->addSql('ALTER TABLE matiere DROP FOREIGN KEY FK_9014574A5EC1162');
        $this->addSql('ALTER TABLE devoir_user DROP FOREIGN KEY FK_78666799C583534E');
        $this->addSql('ALTER TABLE abonnement DROP FOREIGN KEY FK_351268BB77EF1B1E');
        $this->addSql('ALTER TABLE classe DROP FOREIGN KEY FK_8F87BF9677EF1B1E');
        $this->addSql('ALTER TABLE jumellage DROP FOREIGN KEY FK_AEF28FB77EF1B1E');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D64977EF1B1E');
        $this->addSql('ALTER TABLE abonnement DROP FOREIGN KEY FK_351268BB2A68F4D1');
        $this->addSql('ALTER TABLE devoir DROP FOREIGN KEY FK_749EA771F46CD258');
        $this->addSql('ALTER TABLE notification_user DROP FOREIGN KEY FK_35AF9D73EF1A9D84');
        $this->addSql('ALTER TABLE devoir_user DROP FOREIGN KEY FK_78666799A76ED395');
        $this->addSql('ALTER TABLE litige DROP FOREIGN KEY FK_EEE9D46DA76ED395');
        $this->addSql('ALTER TABLE notification_user DROP FOREIGN KEY FK_35AF9D73A76ED395');
        $this->addSql('ALTER TABLE user_visioconference DROP FOREIGN KEY FK_C0546ECCA76ED395');
        $this->addSql('ALTER TABLE user_classe DROP FOREIGN KEY FK_EAD5A4ABA76ED395');
        $this->addSql('ALTER TABLE user_visioconference DROP FOREIGN KEY FK_C0546ECCC5C89FEB');
        $this->addSql('DROP TABLE abonnement');
        $this->addSql('DROP TABLE classe');
        $this->addSql('DROP TABLE cour');
        $this->addSql('DROP TABLE cycle');
        $this->addSql('DROP TABLE devoir');
        $this->addSql('DROP TABLE devoir_user');
        $this->addSql('DROP TABLE ecole');
        $this->addSql('DROP TABLE formule');
        $this->addSql('DROP TABLE jumellage');
        $this->addSql('DROP TABLE litige');
        $this->addSql('DROP TABLE matiere');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE notification_user');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_visioconference');
        $this->addSql('DROP TABLE user_classe');
        $this->addSql('DROP TABLE visioconference');
    }
}
