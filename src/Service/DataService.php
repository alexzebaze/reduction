<?php
namespace App\Service;

use Symfony\Component\Config\Definition\Exception\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Carbon\Carbon;

class DataService{

    public function getAnnee(){
        $years = ['Année'=>""];
        for ($i=1950; $i < 2020 ; $i++) { 
            $years[$i] = ''.$i;
        }
        return $years;
    }
    public function getCarburant(){
        $years = [
            'Caburant'=>'',
            'Essence'=>'essence',
            'Diesel'=>'diesel',
            'Hybrid'=>'hybrid',
            'Electrique'=>'electrique',
            'Gaz'=>'gaz',
        ];
        
        return $years;
    }
    public function getTransmission(){
        $years = [
            'Transmission'=>'',
            'Automatique'=>'automatique',
            'Manuelle'=>'manuelle'
        ];
        
        return $years;
    }

    public function make_thumb($src, $dest, $desired_width, $extension) {

        /* read the source image */
        if(is_file($src)){
            if(strtolower($extension) == "png")
                $source_image = imagecreatefrompng($src);
            elseif(strtolower($extension) == "jpeg")
                $source_image = imagecreatefromjpeg($src);
            elseif(strtolower($extension) == "jpg")
                $source_image = imagecreatefromjpg($src);
            else
                return 1;
            $width = imagesx($source_image);
            $height = imagesy($source_image);

            $desired_height = floor($height * ($desired_width / $width));

            $virtual_image = imagecreatetruecolor($desired_width, $desired_height);

            imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

            imagejpeg($virtual_image, $dest);
        }
        
    }
    public function slugify($string){
        return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
    }
}
