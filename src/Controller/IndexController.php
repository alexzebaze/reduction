<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class IndexController extends AbstractController
{
    
    public function __construct(){
    }

    /**
     * @Route("/", name="index")
     */
    public function vehicules()
    {
        return $this->render('website/vehicules.html.twig', []);
    }

}