<?php

namespace App\Controller;

use App\Entity\Car;
use App\Form\CarType;
use App\Repository\CarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\DataService;

/**
 * @Route("/dashboard/vehicules")
 */
class CarController extends Controller
{
    private $data_s;

    public function __construct(DataService $data_s){
        $this->data_s = $data_s;
    }

    /**
     * @Route("/", name="admin_vehicules", methods={"GET"})
     */
    public function index(CarRepository $carRepository): Response
    {
        return $this->render('admin/car/index.html.twig', [
            'cars' => $carRepository->findBy([],['id'=>'DESC']),
        ]);
    }

    /**
     * @Route("/new", name="car_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $car = new Car();
        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            $this->setOtherField($request, $car);

            $entityManager->persist($car);
            $entityManager->flush();

            return $this->redirectToRoute('admin_vehicules');
        }

        return $this->render('admin/car/new.html.twig', [
            'car' => $car,
            'form' => $form->createView(),
        ]);
    }

    public function setOtherField($request, $car){
        if($request->request->get('prix'))
            $car->setPrix(str_replace(",", '.', $request->request->get('prix')));
        if($request->request->get('killometrage'))
            $car->setKillometrage(str_replace(",", '.', $request->request->get('killometrage')));
        if($request->request->get('etat'))
            $car->setEtat(str_replace(",", '.', $request->request->get('etat')));
        $car->setStatus($request->request->get('status'));

        $thumbnail = $request->files->get('thumbnail');
        if ($thumbnail) {
            $dir = $this->get('kernel')->getProjectDir() . "/public/assets/images/" . date('Y-m') . "/";

            $compressed_dir = $this->get('kernel')->getProjectDir() . "/public/assets/images/" . date('Y-m') . "/compressed/";
            try {
                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                }
                if (!is_dir($compressed_dir)) {
                    mkdir($compressed_dir, 0777, true);
                }
            } catch (FileException $e) {
            }

            $originalFilename = pathinfo($thumbnail->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $this->data_s->slugify($originalFilename);
            $extension = $thumbnail->guessExtension();
            $newFilename = $safeFilename . '-' . uniqid() . '.' . $thumbnail->guessExtension();
           $thumbnail->move($dir, $newFilename);

           $this->data_s->make_thumb($dir.$newFilename, $compressed_dir.$newFilename,200,$extension);
            
            $car->setThumb($newFilename);
        }  

        $galeries = $request->files->get('galeries');
        if ($galeries) {
            $tabImage = [];
            $dir = $this->get('kernel')->getProjectDir() . "/public/assets/images/" . date('Y-m') . "/";

            $compressed_dir = $this->get('kernel')->getProjectDir() . "/public/assets/images/" . date('Y-m') . "/compressed/";
            try {
                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                }
                if (!is_dir($compressed_dir)) {
                    mkdir($compressed_dir, 0777, true);
                }
            } catch (FileException $e) {
            }

            foreach ($galeries as $value) {
                $originalFilename = pathinfo($value->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $this->data_s->slugify($originalFilename);
                $extension = $value->guessExtension();
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $value->guessExtension();
               $value->move($dir, $newFilename);

               $tabImage[] = $newFilename;
               $this->data_s->make_thumb($dir.$newFilename, $compressed_dir.$newFilename,200,$extension);
            }

            $carGallery = [];
            if(!is_null($car->getGallery())){
                $carGallery = unserialize($car->getGallery());
            }

            $car->setGallery(serialize( array_merge($carGallery, $tabImage) ));
        }
        return $car;
    }

    /**
     * @Route("/{id}", name="car_show", methods={"GET"})
     */
    public function show(Car $car): Response
    {
        return $this->render('admin/car/show.html.twig', [
            'car' => $car,
        ]);
    }

    /**
     * @Route("/{id}/delete-images", name="car_delete_images", methods={"GET"})
     */
    public function deletePhotos(Request $request, Car $car): Response
    {
        $image = $request->query->get('image');
        $galeries = unserialize($car->getGallery());
        if(array_search($image, $galeries) !== FALSE){
            unset($galeries[array_search($image, $galeries)]);
        }
        $car->setGallery(serialize($galeries));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->flush();
        
        return $this->redirectToRoute('car_edit', ['id'=>$car->getId()]);
    }    

    /**
     * @Route("/{id}/edit", name="car_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Car $car): Response
    {
        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->setOtherField($request, $car);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_vehicules');
        }

        return $this->render('admin/car/edit.html.twig', [
            'car' => $car,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="car_delete", methods={"POST"})
     */
    public function delete(Request $request, Car $car): Response
    {
        if ($this->isCsrfTokenValid('delete'.$car->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($car);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_vehicules');
    }
}
