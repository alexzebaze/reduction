<?php

namespace App\Form;

use App\Entity\Car;
use App\Entity\Model;
use App\Entity\Marque;
use Symfony\Component\Form\AbstractType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Service\DataService;

class CarType extends AbstractType
{
    private $data_s;

    public function __construct(DataService $data_s){
        $this->data_s = $data_s;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, array(
                'label' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('description', TextareaType::class, array(
                'label' => false,
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('carburant', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'choices' => $this->data_s->getCarburant(),
                'attr' => array(
                    'class' => 'form-control'
                )
            ])
            ->add('transmission', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'choices' => $this->data_s->getTransmission(),
                'attr' => array(
                    'class' => 'form-control'
                )
            ])
            ->add('annee', ChoiceType::class, [
                'label' => false,
                'required' => false,
                'choices' => $this->data_s->getAnnee(),
                'attr' => array(
                    'class' => 'form-control'
                )
            ])
            ->add('tag', TextType::class, array(
                'label' => false,
                'required' => false,
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('marque', EntityType::class, array(
                'class' => Marque::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->orderBy('m.nom', 'ASC');
                },
                'required' => false,
                'label' => false,
                'choice_label' => 'nom',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('model', EntityType::class, array(
                'class' => Model::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('m')
                        ->orderBy('m.nom', 'ASC');
                },
                'required' => false,
                'label' => false,
                'choice_label' => 'nom',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Car::class,
        ]);
    }
}
